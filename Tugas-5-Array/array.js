////////////////////////////////////////////////////////////////////////////
////                            soal 1                                  ////
////////////////////////////////////////////////////////////////////////////

function range(num1, num2) {
  var container = "";

  if (num1 > num2) {
    while (num1 >= num2) {
      container += num1 + ",";
      num1--;
    }
    var hasil = container.split(",").map(Number);
    hasil.pop();
  } else if (num1 < num2) {
    while (num1 <= num2) {
      container += num1 + ",";
      num1++;
    }
    var hasil = container.split(",").map(Number);
    hasil.pop();
  } else {
    container += -1;
    var hasil = container;
  }

  return hasil;
}

var num1 = 10;
var num2 = 5;

console.log(range(num1, num2));
console.log(
  "========================================================================="
);

////////////////////////////////////////////////////////////////////////////
////                            soal 2                                  ////
////////////////////////////////////////////////////////////////////////////

function rangeWithStep(num1, num2, num3) {
  var container = "";

  if (num3 == null) {
    container += -1;
    var hasil = container;
  } else {
    if (num1 > num2) {
      while (num1 >= num2) {
        container += num1 + ",";
        num1 -= num3;
      }
      var hasil = container.split(",").map(Number);
      hasil.pop();
    } else if (num1 < num2) {
      while (num1 <= num2) {
        container += num1 + ",";
        num1 += num3;
      }
      var hasil = container.split(",").map(Number);
      hasil.pop();
    } else {
      container += -1;
      var hasil = container;
    }
  }

  return hasil;
}

var num1 = 1;
var num2 = 20;
var num3 = 3;

console.log(rangeWithStep(num1, num2, num3));
console.log(
  "========================================================================="
);

////////////////////////////////////////////////////////////////////////////
////                            soal 3                                  ////
////////////////////////////////////////////////////////////////////////////

function sum(num1, num2, num3) {
  var container = "";
  var jumlah = 0;

  if (num1 == null && num2 == null && num3 == null) {
    jumlah = 0;
  } else {
    if (num1 > num2) {
      while (num1 >= num2) {
        container += num1 + ",";
        if (num3 == null) {
          num1--;
        } else {
          num1 -= num3;
        }
      }
      var hasil = container.split(",").map(Number);
      hasil.pop();
      for (var count = 0; count < hasil.length; count++) {
        jumlah += hasil[count];
      }
    } else if (num1 < num2) {
      while (num1 <= num2) {
        container += num1 + ",";
        if (num3 == null) {
          num1++;
        } else {
          num1 += num3;
        }
      }
      var hasil = container.split(",").map(Number);
      hasil.pop();
      for (var count = 0; count < hasil.length; count++) {
        jumlah += hasil[count];
      }
    } else {
      jumlah = 1;
    }
  }

  return jumlah;
}

var num1 = 1;
var num2 = 10;
var num3 = 2;

console.log(sum(num1, num2, num3));
console.log(
  "========================================================================="
);

////////////////////////////////////////////////////////////////////////////
////                            soal 4                                  ////
////////////////////////////////////////////////////////////////////////////

function dataHandling(input) {
  var container = "";
  for (var count = 0; count < input.length; count++) {
    container +=
      "Nomor ID: " +
      input[count][0] +
      "\n" +
      "Nama Lengkap: " +
      input[count][1] +
      "\n" +
      "TTL: " +
      input[count][2] +
      ", " +
      input[count][3] +
      "\n" +
      "Hobi: " +
      input[count][4] +
      "\n" +
      "\n";
  }
  return container;
}

var input = [
  ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
  ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
  ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
  ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"],
];

console.log(dataHandling(input));
console.log(
  "========================================================================="
);

////////////////////////////////////////////////////////////////////////////
////                            soal 5                                  ////
////////////////////////////////////////////////////////////////////////////

function balikKata(input) {
  var balik = "";
  for (var count = input.length - 1; count >= 0; count--) {
    balik += input[count];
  }
  return balik;
}

var input = "Riala";
console.log(balikKata(input));
console.log(
  "========================================================================="
);

////////////////////////////////////////////////////////////////////////////
////                            soal 6                                  ////
////////////////////////////////////////////////////////////////////////////

function dataHandling2(input) {
  // add & remove array
  input.splice(1, 1, "Roman Alamsyah Elsharawy");
  input.splice(2, 1, "Provinsi Bandar Lampung");
  input.splice(4, 1, "Pria");
  input.push("SMA Internasional Metro");
  console.log(input);
  //take month value
  var bulan = input[3].split("/");
  switch (bulan[1]) {
    case "01": {
      console.log("Januari");
      break;
    }
    case "02": {
      console.log("Februari");
      break;
    }
    case "03": {
      console.log("Maret");
      break;
    }
    case "04": {
      console.log("April");
      break;
    }
    case "05": {
      console.log("Mei");
      break;
    }
    case "06": {
      console.log("Juni");
      break;
    }
    case "07": {
      console.log("Juli");
      break;
    }
    case "08": {
      console.log("Agustus");
      break;
    }
    case "09": {
      console.log("September");
      break;
    }
    case "10": {
      console.log("Oktober");
      break;
    }
    case "11": {
      console.log("November");
      break;
    }
    case "12": {
      console.log("Desember");
      break;
    }
    default: {
      console.log("input bulan salah");
    }
  }
  //sorting
  var salinsort = bulan.slice();
  salinsort.sort((a, b) => b - a);
  console.log(salinsort);
  //join
  var salinjoin = bulan.slice();
  var join = salinjoin.join("-");
  console.log(join);
  //slice
  var slice = input[1].slice(0, 14);
  console.log(slice);
}

var input = [
  "0001",
  "Roman Alamsyah ",
  "Bandar Lampung",
  "21/05/1989",
  "Membaca",
];
dataHandling2(input);
