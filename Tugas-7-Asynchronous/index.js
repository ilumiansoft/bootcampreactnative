const readBooks = require("./callback.js");

const books = [
  { name: "LOTR", timeSpent: 3000 },
  { name: "Fidas", timeSpent: 2000 },
  { name: "Kalkulus", timeSpent: 4000 },
];

var waktu = 10000;
var count = 0;

function membaca() {
  if (count <= books.length - 1 && waktu >= books[count].timeSpent) {
    readBooks(waktu, books[count], function callback(check) {
      count++;
      var selisih = waktu - check;
      waktu -= selisih;
      membaca();
    });
  }
}

membaca();
