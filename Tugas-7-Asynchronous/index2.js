const readBooksPromise = require("./promise.js");

const books = [
  { name: "LOTR", timeSpent: 3000 },
  { name: "Fidas", timeSpent: 2000 },
  { name: "Kalkulus", timeSpent: 4000 },
];

var limitWaktu = 10000;
var count = 0;

function membacaPromise() {
  if (count <= books.length - 1 && limitWaktu >= books[count].timeSpent) {
    readBooksPromise(limitWaktu, books[count])
      .then(function (resolve) {
        count++;
        var selisih = limitWaktu - resolve;
        limitWaktu -= selisih;
        membacaPromise();
      })
      .catch(function (reject) {});
  }
}

membacaPromise();
