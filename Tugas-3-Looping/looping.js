///////////////////////////////////////////////////////////////////////////////////////
////                                soal while                                     ////
///////////////////////////////////////////////////////////////////////////////////////

// var counter1 = 0;
// var counter2 = 40;
// var title1 = false;
// var title2 = false;

// while (counter1 < 20) {
//   if (title1) {
//     counter1 += 2;
//     counter2 -= 2;
//     console.log(counter1 + " - I love coding");
//     while (counter2 <= 20 && counter2 >= 2) {
//       if (title2) {
//         console.log(counter2 + " - I will become a mobile developer");
//         counter2 -= 2;
//       } else {
//         console.log("LOOPING KEDUA ");
//         title2 = true;
//       }
//     }
//   } else {
//     console.log("LOOPING PERTAMA");
//     title1 = true;
//   }
// }

///////////////////////////////////////////////////////////////////////////////////////
////                                soal for                                       ////
///////////////////////////////////////////////////////////////////////////////////////

// for (var angka = 1; angka <= 20; angka++) {
//   if (angka % 2 == 0) {
//     console.log(angka + " - Berkualitas");
//   } else {
//     if (angka % 3 == 0) {
//       console.log(angka + " - I love coding");
//     } else {
//       console.log(angka + " - Santai");
//     }
//   }
// }

///////////////////////////////////////////////////////////////////////////////////////
////                          soal Membuat Persegi Panjang #                       ////
///////////////////////////////////////////////////////////////////////////////////////

// var hitung = 0;
// var baris = 1;
// while (hitung <= 8 && baris <= 4) {
//   process.stdout.write("#");
//   hitung++;
//   while (hitung == 8 && baris <= 4) {
//     console.log("");
//     hitung = 0;
//     baris++;
//   }
// }

///////////////////////////////////////////////////////////////////////////////////////
////                               soal Membuat Tangga #                           ////
///////////////////////////////////////////////////////////////////////////////////////

// var hitung = 0;
// var baris = 1;
// var kolom = 1;
// while (hitung <= kolom && baris <= 7) {
//   process.stdout.write("#");
//   hitung++;
//   while (hitung >= kolom && baris <= 7) {
//     console.log("");
//     hitung = 0;
//     kolom++;
//     baris++;
//   }
// }

///////////////////////////////////////////////////////////////////////////////////////
////                              soal Membuat Papan Catur #                       ////
///////////////////////////////////////////////////////////////////////////////////////

var baris = 1;
var limit = 0;
var pattern = false;

while (limit <= 8 && baris <= 8) {
  if (baris % 2 == 0) {
    if (pattern) {
      process.stdout.write(" ");
      pattern = false;
      limit++;
    } else {
      process.stdout.write("#");
      pattern = true;
      limit++;
    }
  } else {
    if (pattern) {
      process.stdout.write("#");
      pattern = false;
      limit++;
    } else {
      process.stdout.write(" ");
      pattern = true;
      limit++;
    }
  }

  while (limit == 8 && baris <= 8) {
    console.log("");
    limit = 0;
    baris++;
    pattern = false;
  }
}
