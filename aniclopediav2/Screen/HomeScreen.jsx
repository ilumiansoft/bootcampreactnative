import React, { useContext, useState, useEffect } from "react";
import {
    StyleSheet,
    Text,
    View,
    Image,
    TextInput,
    TouchableOpacity,
    ToastAndroid,
} from "react-native";
import { StackActions } from "@react-navigation/native";
import Ionicons from '@expo/vector-icons/Ionicons';
import axios from "axios";
import * as firebase from "firebase";

const colors = {
    pink: "#e5abb7",
    pinkTua: "#b76281",
    unguTua: "#36376f",
    ungu: "#676ebc",
    putih: "#f7f7f7",
    biru: "#bee3f5",
    biruTua: "#93bee9",
};

const URL = 'https://api.aniapi.com/v1/anime/';


export default function HomeScreen({ navigation }) {

    const [search, setSearch] = useState("");
    const [img1, setImg1] = useState();
    const [img2, setImg2] = useState();
    const [img3, setImg3] = useState();
    const [tit1, setTit1] = useState();
    const [tit2, setTit2] = useState();
    const [tit3, setTit3] = useState();
    const [id1, setId1] = useState();
    const [id2, setId2] = useState();
    const [id3, setId3] = useState();

    const firebaseConfig = {
        apiKey: "AIzaSyBZ9OFzTBsJZBhpD7t44ZKXv-UMEZyM5tE",
        authDomain: "jcc-batch2-latihanauth.firebaseapp.com",
        projectId: "jcc-batch2-latihanauth",
        storageBucket: "jcc-batch2-latihanauth.appspot.com",
        messagingSenderId: "567229358169",
        appId: "1:567229358169:web:8639abb106fc4776d7d148",
    };

    if (!firebase.apps.length) {
        firebase.initializeApp(firebaseConfig);
    }

    const callApi = async () => {
        for (let i = 0; i < 3; i++) {
            let random = Math.floor(Math.random() * 100) + 1;
            let randomStr = '' + random
            const response = await axios.get(URL + randomStr);
            const dataImg = response.data.data.cover_image;
            const dataTit = response.data.data.titles.rj;
            const dataId = response.data.data.id;
            const dataIdStr = '' + dataId
            switch (i) {
                case 0: { setImg1(dataImg); setTit1(dataTit); setId1(dataIdStr); break; }
                case 1: { setImg2(dataImg); setTit2(dataTit); setId2(dataIdStr); break; }
                case 2: { setImg3(dataImg); setTit3(dataTit); setId3(dataIdStr); break; }
            }
        }

    };

    useEffect(() => {
        callApi();
    }, []);

    const onSearch = () => {
        if (search == "") {
            ToastAndroid.show('Please type something first', ToastAndroid.SHORT);
        } else {
            navigation.navigate("Search", { search: search });
        }
    }

    const onLogout = () => {
        firebase
            .auth()
            .signOut()
            .then(() => {
                console.log("logout berhasil");
                ToastAndroid.show("Berhasil Logout", ToastAndroid.SHORT);
                navigation.dispatch(StackActions.replace('Login'));
            })
            .catch((e) => {
                console.log("logout gagal " + e);
                ToastAndroid.show("Logout Gagal", ToastAndroid.SHORT);
            });
    };

    return (
        <View
            style={[
                styles.container,
                {
                    flexDirection: "column",
                },
            ]}
        >
            <View style={styles.containerTitle}>
                <Image style={styles.homeLogo} source={require("../image/img_title.png")} />
                <TouchableOpacity style={styles.backButtonBox} onPress={() => { onLogout(); }}>
                    <Ionicons name="log-out-outline" size={50} color="#676ebc" />
                </TouchableOpacity>
            </View>

            <Text style={styles.headerText}>GOT THE TITLE IN MIND?</Text>
            <Text style={styles.headerText}>HIT THE SEARCH BUTTON BELLOW!</Text>
            <View style={styles.containerSearch}>
                <TextInput
                    style={styles.inputSearch}
                    placeholder="Type anime title in here"
                    value={search}
                    onChangeText={(value) => setSearch(value)}
                />
                <TouchableOpacity onPress={onSearch}>
                    <Ionicons name="search" size={25} color="#676ebc" />
                </TouchableOpacity>

            </View>
            <Text style={styles.headerText2}>RANDOM TITLE</Text>
            <View style={styles.lineBetween} />
            <View style={styles.containerList}>
                <TouchableOpacity style={styles.containerImg} onPress={() => { navigation.navigate('Detail', { aniID: id1 }) }}>
                    <Image style={styles.imgList} source={{ uri: img1 }} />
                    <Text style={{ color: colors.ungu, flex: 1, flexWrap: "wrap", fontSize: 13, }}>{tit1}</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.containerImg} onPress={() => { navigation.navigate("Detail", { aniID: id2 }); }}>
                    <Image style={styles.imgList} source={{ uri: img2 }} />
                    <Text style={{ color: colors.ungu, flex: 1, flexWrap: "wrap", fontSize: 13, }}>{tit2}</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.containerImg} onPress={() => { navigation.navigate("Detail", { aniID: id3 }); }}>
                    <Image style={styles.imgList} source={{ uri: img3 }} />
                    <Text style={{ color: colors.ungu, flex: 1, flexWrap: "wrap", fontSize: 13, }}>{tit3}</Text>
                </TouchableOpacity>
            </View>
            <TouchableOpacity style={styles.fab} onPress={() => { navigation.navigate("About"); }}>
                <Ionicons name="person" size={50} color="#f7f7f7" />
            </TouchableOpacity>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.putih,
    },
    containerSearch: {
        margin: 20,
        alignItems: "center",
        flexDirection: 'row',
        borderBottomWidth: 1,
        borderColor: colors.unguTua,
        paddingBottom: 5,
    },
    containerTitle: {
        flexDirection: "row",
        justifyContent: "space-between",
        alignContent: "center",
        alignItems: 'center',

    },
    containerList: {
        flexDirection: "row",
        justifyContent: "space-evenly",
        alignContent: "center",
        alignItems: 'center',
    },
    containerImg: {
        flex: 1,
        height: 200,
        alignItems: 'center',
        borderWidth: 1,
        borderRadius: 5,
        borderColor: colors.unguTua,
        padding: 10,
        marginHorizontal: 10,
        marginTop: 20,
    },
    imgList: {
        flex: 2,
        height: 100,
        width: 100,
        margin: 20,
        padding: 10,
        resizeMode: "contain",
        alignSelf: "center",
    },
    inputSearch: {
        flex: 1,
    },
    homeLogo: {
        alignSelf: "flex-start",
        height: 150,
        width: 250,
        marginHorizontal: 20,
        resizeMode: "contain",
    },
    headerText: {
        fontSize: 14,
        textAlign: "center",
        fontWeight: "bold",
        alignSelf: "center",
        color: colors.ungu,
        marginHorizontal: 20,
    },
    headerText2: {
        fontSize: 18,
        fontWeight: "bold",
        color: colors.ungu,
        marginHorizontal: 20,
        marginTop: 30,
    },
    lineBetween: {
        borderBottomColor: colors.unguTua,
        borderBottomWidth: 1,
        marginLeft: 20,
        width: "70%",
    },
    notifText: {
        fontSize: 12,
        textAlign: "center",
        color: colors.ungu,
        marginHorizontal: 10,
    },
    input: {
        height: 50,
        margin: 12,
        borderWidth: 1,
        borderRadius: 15,
        borderColor: colors.ungu,
        padding: 10,
        marginTop: 10,
    },
    buttonBox: {
        height: 40,
        margin: 12,
        borderRadius: 15,
        backgroundColor: colors.ungu,
        padding: 10,
        marginTop: 30,
        marginBottom: 25,
    },
    backButtonBox: {
        height: 70,
        width: 70,
        margin: 12,
        marginTop: 10,
        padding: 10,
    },
    buttonBox2: {
        marginLeft: -13,
    },
    buttonText: {
        fontSize: 14,
        fontWeight: "bold",
        alignSelf: "center",
        color: colors.putih,
        marginHorizontal: 10,
    },
    fab: {
        position: "absolute",
        bottom: 20,
        right: 20,
        padding: 15,
        borderRadius: 50,
        backgroundColor: colors.ungu,
    },
});
