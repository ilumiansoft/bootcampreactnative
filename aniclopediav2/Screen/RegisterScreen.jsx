import React, { useContext, useState } from "react";
import {
    StyleSheet,
    Text,
    View,
    Image,
    TextInput,
    TouchableOpacity,
    ToastAndroid,
} from "react-native";

import * as firebase from "firebase";

const colors = {
    pink: "#e5abb7",
    pinkTua: "#b76281",
    unguTua: "#36376f",
    ungu: "#676ebc",
    putih: "#f7f7f7",
    biru: "#bee3f5",
    biruTua: "#93bee9",
};


export default function RegisterScreen({ navigation }) {

    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [error, setError] = useState("");

    const firebaseConfig = {
        apiKey: "AIzaSyBZ9OFzTBsJZBhpD7t44ZKXv-UMEZyM5tE",
        authDomain: "jcc-batch2-latihanauth.firebaseapp.com",
        projectId: "jcc-batch2-latihanauth",
        storageBucket: "jcc-batch2-latihanauth.appspot.com",
        messagingSenderId: "567229358169",
        appId: "1:567229358169:web:8639abb106fc4776d7d148",
    };

    const onRegister = () => {
        if (email != "" && password != "") {
            firebase
                .auth()
                .createUserWithEmailAndPassword(email, password)
                .then(() => {
                    ToastAndroid.show(
                        "Registered successfully",
                        ToastAndroid.SHORT
                    );
                    setEmail("");
                    setPassword("");
                    navigation.navigate("Login");
                    console.log("Registered successfully");
                })
                .catch((e) => {
                    console.log(e.code);
                    switch (e.code) {
                        case "auth/email-already-in-use": {
                            setError("E-MAIL ALREADY REGISTERED");
                            setEmail("");
                            setPassword("");
                            break;
                        }
                        case "auth/invalid-email": {
                            setError("PLEASE INPUT A VALID E-MAIL");
                            setEmail("");
                            setPassword("");
                            break;
                        }
                        case "auth/weak-password": {
                            setError("PLEASE INPUT 6 OR MORE CHARACTER FOR PASSWORD");
                            setEmail("");
                            setPassword("");
                            break;
                        }
                        default: { console.log('no error'); }
                    }
                });
        } else {
            setError("PLEASE FILL THE EMAIL AND PASSWORD BOX");
        }
    };

    if (!firebase.apps.length) {
        firebase.initializeApp(firebaseConfig);
    }

    return (
        <View
            style={[
                styles.container,
                {
                    flexDirection: "column",
                },
            ]}
        >
            <View style={styles.container}>
                <Text style={styles.headerText}>CREATE AN ACCOUNT</Text>
                <TextInput
                    style={styles.input}
                    placeholder="E-mail"
                    value={email}
                    onChangeText={(email) => setEmail(email)}
                />
                <TextInput
                    style={styles.input}
                    placeholder="Password"
                    value={password}
                    onChangeText={(password) => setPassword(password)}
                />
                <TouchableOpacity style={styles.buttonBox} onPress={onRegister}>
                    <Text style={styles.buttonText}>REGISTER</Text>
                </TouchableOpacity>
                <Text style={styles.notifText}>{error}</Text>
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: "center",
        backgroundColor: colors.putih,
    },
    containerReg: {
        flexDirection: "row",
        justifyContent: "center",
    },
    loginLogo: {
        height: 200,
        margin: 200,
        marginVertical: 70,
        resizeMode: "contain",
        alignSelf: "center",
    },
    headerText: {
        fontSize: 14,
        fontWeight: "bold",
        color: colors.ungu,
        marginHorizontal: 10,
    },
    notifText: {
        fontSize: 12,
        textAlign: "center",
        color: colors.pinkTua,
        marginHorizontal: 10,
    },
    input: {
        height: 50,
        margin: 12,
        borderWidth: 1,
        borderRadius: 15,
        borderColor: colors.ungu,
        padding: 10,
        marginTop: 10,
    },
    buttonBox: {
        height: 40,
        margin: 12,
        borderRadius: 15,
        backgroundColor: colors.ungu,
        padding: 10,
        marginTop: 30,
        marginBottom: 25,
    },
    buttonBox2: {
        marginLeft: -13,
    },
    buttonText: {
        fontSize: 14,
        fontWeight: "bold",
        alignSelf: "center",
        color: colors.putih,
        marginHorizontal: 10,
    },
});
