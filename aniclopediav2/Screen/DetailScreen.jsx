import React, { useContext, useState, useEffect } from "react";
import {
    StyleSheet,
    Text,
    View,
    Image,
    TouchableOpacity,
    ToastAndroid,
    FlatList,
    Linking,
} from "react-native";
import Ionicons from '@expo/vector-icons/Ionicons';
import axios from "axios";

const URLAnime = 'https://api.aniapi.com/v1/anime/';
const URLSong = 'https://api.aniapi.com/v1/song?anime_id=';

const colors = {
    pink: "#e5abb7",
    pinkTua: "#b76281",
    unguTua: "#36376f",
    ungu: "#676ebc",
    putih: "#f7f7f7",
    biru: "#bee3f5",
    biruTua: "#93bee9",
};

export default function DetailScreen({ route, navigation }) {

    const { aniID } = route.params;
    const [pic, setPic] = useState();
    const [title, setTitle] = useState();
    const [desc, setDesc] = useState();
    const [data, setData] = useState();
    const [isDataExist, setIsDataExist] = useState();


    const callApi = async () => {

        const responseAnime = await axios.get(URLAnime + aniID);
        const dataImg = responseAnime.data.data.cover_image;
        setPic(dataImg);
        const dataTit = responseAnime.data.data.titles.rj;
        setTitle(dataTit);
        const dataDesc = responseAnime.data.data.descriptions.en;
        setDesc(dataDesc);
        const responseSong = await axios.get(URLSong + aniID);
        const dataSong = responseSong.data.data.documents;
        if (dataSong == null) {
            setIsDataExist(false);
        } else {
            setData(dataSong);
            setIsDataExist(true);
        }
    };

    useEffect(() => {
        callApi();
    }, []);


    return (
        <View
            style={[
                styles.container,
                {
                    flex: 1,
                    flexDirection: "column",
                    justifyContent: "flex-start",
                },
            ]}
        >
            {/* header atas */}
            <View
                style={styles.container}
                flexDirection="row"
                justifyContent="space-between"
                alignItems='center'
            >
                <View>
                    <Text style={styles.headerText}>DETAIL SCREEN</Text>
                    <View style={styles.lineBetween} />
                </View>
                <TouchableOpacity style={styles.buttonBox} onPress={() => { navigation.goBack(); }}>
                    <Ionicons name="arrow-back-circle" size={30} color="#676ebc" />
                </TouchableOpacity>
            </View >
            {/* hasil search - Anime API part*/}
            <View style={[styles.container,
            {
                flex: 0.5,
                flexDirection: "row",
                justifyContent: "space-between",
                alignItems: 'center',
                marginBottom: 30,
            },]}>
                <Image style={styles.image} source={{ uri: pic }} />
                <View style={styles.containerDesc}>
                    <Text style={styles.titleAnimeText}>{title}</Text>
                    <Text style={styles.descText} numberOfLines={10}>{desc}</Text>
                </View>

            </View>
            {/* hasil search - Anime Song part*/}
            {isDataExist ? (<FlatList style={styles.containerMusic}
                data={data}
                horizontal={false}
                renderItem={({ item }) => (
                    <TouchableOpacity
                        style={styles.containerItem}
                        onPress={() => { }}
                    >
                        <View>
                            <Text style={styles.titleText}>{item.title}</Text>
                            <Text style={styles.artistText}>{item.artist}</Text>
                        </View>
                        <TouchableOpacity onPress={() => { Linking.openURL(item.open_spotify_url) }}>
                            <Ionicons name="play" size={30} color="#676ebc" />
                        </TouchableOpacity>
                    </TouchableOpacity>
                )}
            />) : (
                <View style={styles.containerEmpty}>
                    <Text style={styles.warnText}>Sorry!, We cant find Song associated with this title in our database, please try another title</Text>
                </View>
            )}

        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: colors.putih,
    },
    containerMusic: {
        flex: 3,
    },
    containerTitle: {
        flex: 1,
        margin: 15,
        alignItems: "flex-start",
        justifyContent: "space-between",
        flexDirection: 'row',
        borderBottomWidth: 2,
        width: '50%',
        borderColor: colors.unguTua,
        paddingBottom: 5,
    },
    containerItem: {
        flex: 1,
        margin: 15,
        alignItems: "flex-start",
        justifyContent: "space-between",
        flexDirection: 'row',
        borderBottomWidth: 1,
        borderColor: colors.unguTua,
        paddingBottom: 5,
    },
    containerEmpty: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    image: {
        flex: 1,
        height: 200,
        width: 200,
        marginHorizontal: 20,
        resizeMode: "contain",
        alignSelf: "flex-start",
    },
    headerText: {
        fontSize: 18,
        fontWeight: "bold",
        color: colors.ungu,
        marginHorizontal: 20,
        marginTop: 30,
    },
    titleText: {
        fontSize: 14,
        fontWeight: "bold",
        color: colors.ungu,
    },
    warnText: {
        fontSize: 14,
        textAlign: "center",
        fontWeight: "bold",
        color: colors.ungu,
    },
    containerDesc: {
        flex: 1,
        marginRight: 20,
        alignItems: "flex-start",
        flexWrap: "wrap",
    },
    titleAnimeText: {
        flex: 0.5,
        textAlign: "center",
        alignSelf: "center",
        fontSize: 14,
        fontWeight: "bold",
        marginBottom: 10,
        color: colors.unguTua,
    },
    descText: {
        flex: 1,
        textAlign: "justify",
        fontSize: 12,
        color: colors.ungu,
    },
    artistText: {
        fontSize: 12,
        fontWeight: "100",
        color: colors.unguTua,
    },
    lineBetween: {
        borderBottomColor: colors.ungu,
        borderBottomWidth: 3,
        marginLeft: 20,
        width: "100%",
    },
    buttonBox: {
        height: 50,
        width: 50,
        margin: 12,
        marginTop: 40,
        padding: 10,
    },
    containerButton: {
        width: 80,
        justifyContent: "space-between",
        flexDirection: "row",
    },
});
