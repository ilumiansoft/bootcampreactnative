import React, { useContext, useState, useEffect } from "react";
import {
    StyleSheet,
    Text,
    View,
    Image,
    TouchableOpacity,
    ToastAndroid,
    FlatList,
    Linking,
} from "react-native";
import Ionicons from '@expo/vector-icons/Ionicons';
import axios from "axios";

const URLAnime = 'https://api.aniapi.com/v1/anime?title=';

const colors = {
    pink: "#e5abb7",
    pinkTua: "#b76281",
    unguTua: "#36376f",
    ungu: "#676ebc",
    putih: "#f7f7f7",
    biru: "#bee3f5",
    biruTua: "#93bee9",
};

export default function SearchScreen({ route, navigation }) {

    const { search } = route.params;
    const [pic, setPic] = useState();
    const [title, setTitle] = useState();
    const [desc, setDesc] = useState();
    const [data, setData] = useState();
    const [isDataExist, setIsDataExist] = useState();


    const callApi = async () => {

        const responseAnime = await axios.get(URLAnime + search);
        const dataAnime = responseAnime.data.data.documents;
        if (dataAnime == null) {
            console.log('masuk');
            setIsDataExist(false);
        } else {
            setData(dataAnime)
            setIsDataExist(true);
        }
    };

    useEffect(() => {
        callApi();
    }, []);


    return (
        <View
            style={[
                styles.container,
                {
                    flex: 1,
                    flexDirection: "column",
                    justifyContent: "flex-start",
                },
            ]}
        >
            {/* header atas */}
            <View
                style={styles.container}
                flexDirection="row"
                justifyContent="space-between"
                alignItems='center'
            >
                <View>
                    <Text style={styles.headerText}>SEARCH RESULT</Text>
                    <View style={styles.lineBetween} />
                </View>
                <TouchableOpacity style={styles.buttonBox} onPress={() => { navigation.goBack(); }}>
                    <Ionicons name="arrow-back-circle" size={30} color="#676ebc" />
                </TouchableOpacity>
            </View >
            {/* hasil search*/}
            {isDataExist ? (<FlatList
                data={data}
                horizontal={false}
                renderItem={({ item }) => (
                    <TouchableOpacity
                        style={styles.containerTitle}
                        onPress={() => { navigation.navigate('Detail', { aniID: item.id }); }}
                    >

                        <View style={styles.containerDesc}>
                            <Image style={styles.image} source={{ uri: item.cover_image }} />
                            <Text style={styles.titleAnimeText}>{item.titles.rj}</Text>
                        </View>
                        <View style={styles.containerDesc}>
                            <Text style={styles.descTextTitle}>Description:</Text>
                            <Text style={styles.descText} numberOfLines={12}>{item.descriptions.en}</Text>
                        </View>

                    </TouchableOpacity>
                )}
            />) : (
                <View style={styles.containerEmpty}>
                    <Text style={styles.warnText}>Theres no Anime that match your search input</Text>
                </View>
            )}

        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: colors.putih,
    },
    containerTitle: {
        flex: 1,
        margin: 15,
        height: 250,
        justifyContent: "center",
        alignContent: "center",
        flexDirection: 'row',
        borderBottomWidth: 1,
        borderColor: colors.ungu,
        paddingBottom: 15,
    },
    containerEmpty: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    image: {
        flex: 3.5,
        height: 150,
        width: 150,
        resizeMode: "contain",
        alignSelf: "center",
    },
    headerText: {
        fontSize: 18,
        fontWeight: "bold",
        color: colors.ungu,
        marginHorizontal: 20,
        marginTop: 30,
    },
    warnText: {
        fontSize: 14,
        textAlign: "center",
        fontWeight: "bold",
        color: colors.ungu,
    },
    containerDesc: {
        flex: 1,
        alignItems: "flex-start",
        alignContent: "center",
        flexWrap: "wrap",
    },
    titleAnimeText: {
        flex: 1,
        textAlign: "center",
        alignSelf: "center",
        fontSize: 14,
        fontWeight: "bold",
        marginHorizontal: 10,
        color: colors.unguTua,
    },
    descTextTitle: {
        flex: 1,
        textAlign: "center",
        alignSelf: "flex-start",
        fontSize: 14,
        color: colors.unguTua,
    },
    descText: {
        flex: 7,
        textAlign: "justify",
        fontSize: 12,
        color: colors.ungu,
    },
    lineBetween: {
        borderBottomColor: colors.ungu,
        borderBottomWidth: 3,
        marginLeft: 20,
        width: "100%",
    },
    buttonBox: {
        height: 50,
        width: 50,
        margin: 12,
        marginTop: 40,
        padding: 10,
    },
});
