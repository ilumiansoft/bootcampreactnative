import React, { useContext, useState } from "react";
import {
    StyleSheet,
    Text,
    View,
    Image,
    TextInput,
    TouchableOpacity,
    ToastAndroid,
} from "react-native";
import { StackActions } from "@react-navigation/native";
import Ionicons from '@expo/vector-icons/Ionicons';

import * as firebase from "firebase";

const colors = {
    pink: "#e5abb7",
    pinkTua: "#b76281",
    unguTua: "#36376f",
    ungu: "#676ebc",
    putih: "#f7f7f7",
    biru: "#bee3f5",
    biruTua: "#93bee9",
};


export default function LoginScreen({ navigation }) {

    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [icon, setIcon] = useState("eye");
    const [textEntry, setTextEntry] = useState(true);

    const firebaseConfig = {
        apiKey: "AIzaSyBZ9OFzTBsJZBhpD7t44ZKXv-UMEZyM5tE",
        authDomain: "jcc-batch2-latihanauth.firebaseapp.com",
        projectId: "jcc-batch2-latihanauth",
        storageBucket: "jcc-batch2-latihanauth.appspot.com",
        messagingSenderId: "567229358169",
        appId: "1:567229358169:web:8639abb106fc4776d7d148",
    };

    const onLogin = () => {
        if (email != "" && password != "") {
            firebase
                .auth()
                .signInWithEmailAndPassword(email, password)
                .then(() => {
                    navigation.dispatch(StackActions.replace('Home'));
                    //navigation.navigate("Home", { username: email });
                    console.log("login berhasil");
                })
                .catch((e) => {
                    switch (e.code) {
                        case "auth/invalid-email": {
                            ToastAndroid.show("Please input a valid E-mail", ToastAndroid.SHORT);
                            break;
                        }
                        case "auth/user-not-found": {
                            ToastAndroid.show("E-mail not registered", ToastAndroid.SHORT);
                            break;
                        }
                        case "auth/wrong-password": {
                            ToastAndroid.show("Wrong Password", ToastAndroid.SHORT);
                            break;
                        }
                        default: { console.log(e.code) }
                    }

                });
        } else {
            ToastAndroid.show(
                "Please fill email and password box before logging in",
                ToastAndroid.SHORT
            );
        }
    };

    if (!firebase.apps.length) {
        firebase.initializeApp(firebaseConfig);
    }

    return (
        <View
            style={[
                styles.container,
                {
                    flexDirection: "column",
                },
            ]}
        >
            <Image style={styles.loginLogo} source={require("../image/img_login.png")} />
            <View style={styles.container}>
                <Text style={styles.headerText}>PLEASE LOGIN TO CONTINUE</Text>
                <TextInput
                    style={styles.containerPass}
                    placeholder="E-mail"
                    keyboardType="email-address"
                    value={email}
                    onChangeText={(email) => setEmail(email)}
                />
                <View style={styles.containerPass}>
                    <TextInput
                        style={styles.inputPass}
                        placeholder="Password"
                        secureTextEntry={textEntry}
                        value={password}
                        onChangeText={(password) => setPassword(password)}
                    />
                    <TouchableOpacity onPress={() => {
                        if (icon == 'eye') {
                            setTextEntry(false);
                            setIcon("eye-off");
                        } else {
                            setTextEntry(true);
                            setIcon("eye");

                        }
                    }}>
                        <Ionicons name={icon} size={25} color="black" />
                    </TouchableOpacity>

                </View>

                <TouchableOpacity style={styles.buttonBox} onPress={onLogin}>
                    <Text style={styles.buttonText}>LOGIN</Text>
                </TouchableOpacity>
                <View style={styles.containerReg}>
                    <Text style={styles.notifText}>FIRST TIME OPENING THIS APP? THEN REGISTER IN</Text>
                    <TouchableOpacity style={styles.buttonBox2} onPress={() => navigation.navigate("Register")}>
                        <Text style={styles.headerText}>HERE!</Text>
                    </TouchableOpacity>
                </View>


            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.putih,
    },
    containerReg: {
        flexDirection: "row",
        justifyContent: "center",
        alignItems: 'center',
    },
    loginLogo: {
        height: 200,
        margin: 200,
        marginVertical: 70,
        resizeMode: "contain",
        alignSelf: "center",
    },
    headerText: {
        fontSize: 14,
        fontWeight: "bold",
        color: colors.ungu,
        marginHorizontal: 10,
    },
    notifText: {
        fontSize: 12,
        textAlign: "center",
        color: colors.ungu,
        marginHorizontal: 10,
    },
    input: {
        height: 50,
        margin: 12,
        borderWidth: 1,
        borderRadius: 15,
        borderColor: colors.ungu,
        padding: 10,
        marginTop: 10,
    },
    inputPass: {
        flex: 1,
    },
    containerPass: {
        margin: 15,
        alignItems: "center",
        flexDirection: 'row',
        borderBottomWidth: 1,
        borderColor: colors.ungu,
        paddingBottom: 10,
    },
    buttonBox: {
        height: 40,
        margin: 12,
        borderRadius: 15,
        backgroundColor: colors.ungu,
        padding: 10,
        marginTop: 30,
        marginBottom: 25,
    },
    buttonBox2: {
        marginLeft: -13,
    },
    buttonText: {
        fontSize: 14,
        fontWeight: "bold",
        alignSelf: "center",
        color: colors.putih,
        marginHorizontal: 10,
    },
});
