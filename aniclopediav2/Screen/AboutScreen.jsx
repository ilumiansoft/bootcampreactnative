import React from "react";
import {
    StyleSheet,
    Text,
    View,
    Image,
    TouchableOpacity,
} from "react-native";
import Ionicons from '@expo/vector-icons/Ionicons';

const colors = {
    biru: "#15869E",
    putih: "#E5E5E5",
    birutua: "#165E6D",
    pureputih: "#FFFFFF",
    hitam: '#000000',
};

export default function AboutScreen({ navigation }) {

    return (
        <View
            style={[
                styles.container,
                {
                    flex: 1,
                    flexDirection: "column",
                    justifyContent: "flex-start",
                },
            ]}
        >
            {/* header atas */}
            <View
                style={styles.container}
                flexDirection="row"
                justifyContent="space-between"
                alignItems='center'
            >
                <View>
                    <Text style={styles.headerText}>DEVELOPER PROFILE</Text>
                    <View style={styles.lineBetween} />
                </View>

                <TouchableOpacity style={styles.buttonBox} onPress={() => { navigation.goBack(); }}>
                    <Ionicons name="arrow-back-circle" size={30} color="#000000" />
                </TouchableOpacity>
            </View>
            {/* foto profil */}
            <View style={styles.container}>
                <Image style={styles.fotoProfil} source={require("../image/img_photo.png")} />
                <Text style={styles.nameText}>ILMAN WAHYU FAUZY</Text>
                <Text style={styles.profText}>GAME DEVELOPER</Text>
            </View>
            {/* skill set */}
            <View style={styles.container2}>
                <View>
                    <Text style={styles.headerText}>SKILL LIST</Text>
                    <View style={styles.lineBetween} />
                    <Image
                        style={{ marginTop: 10, alignSelf: "center", width: 300, height: 300, resizeMode: "contain", }}
                        source={require("../image/img_cv.png")}
                    />
                </View>
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: colors.pureputih,
    },
    container2: {
        backgroundColor: colors.pureputih,
    },
    fotoProfil: {
        height: 200,
        width: 200,
        resizeMode: "contain",
        alignSelf: "center",
    },
    headerText: {
        fontSize: 18,
        fontWeight: "bold",
        color: colors.hitam,
        marginHorizontal: 20,
        marginTop: 30,
    },
    nameText: {
        alignSelf: "center",
        fontSize: 14,
        fontWeight: "bold",
        color: colors.hitam,
        marginHorizontal: 20,
        marginTop: 10,
    },
    profText: {
        alignSelf: "flex-end",
        fontSize: 12,
        fontWeight: "100",
        color: colors.hitam,
        marginHorizontal: 20,
        marginRight: 100,
    },
    lineBetween: {
        borderBottomColor: colors.hitam,
        borderBottomWidth: 3,
        marginLeft: 20,
        width: "70%",
    },
    buttonBox: {
        height: 50,
        width: 50,
        margin: 12,
        marginTop: 40,
        padding: 10,
    },
});
