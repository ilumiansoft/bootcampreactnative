import React from "react";
import { StyleSheet } from "react-native";
// import { NavigationContainer } from "@react-navigation/native";
// import { createNativeStackNavigator } from "@react-navigation/native-stack";
// import LoginScreen from "./Tugas/Tugas11/LoginScreen";
// import AboutScreen from "./Tugas/Tugas11/AboutScreen";
// import ListMovie from "./Tugas/Tugas12/State";
// import LifeCycle from "./Tugas/Tugas12/LifeCycle";
// import Tugas13 from "./Tugas/Tugas13/index";
import MainScreen from "./Tugas/Tugas14/MainScreen";
// import Tugas15 from "./Tugas/Tugas15";
// const Stack = createNativeStackNavigator();
import Quiz3 from "./Tugas/Quiz3/index";

export default function App() {
  return (
    // <NavigationContainer>
    //   <Stack.Navigator
    //     initialRouteName="Login"
    //     screenOptions={{ headerShown: false }}
    //   >
    //     <Stack.Screen name="Login" component={LoginScreen} />
    //     <Stack.Screen name="About" component={AboutScreen} />
    //   </Stack.Navigator>
    // </NavigationContainer>
    <MainScreen />
  );
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    color: "white",
    alignItems: "center",
    justifyContent: "center",
  },
});

// const firebaseConfig = {
//   apiKey: "AIzaSyBZ9OFzTBsJZBhpD7t44ZKXv-UMEZyM5tE",
//   authDomain: "jcc-batch2-latihanauth.firebaseapp.com",
//   projectId: "jcc-batch2-latihanauth",
//   storageBucket: "jcc-batch2-latihanauth.appspot.com",
//   messagingSenderId: "567229358169",
//   appId: "1:567229358169:web:8639abb106fc4776d7d148"
// };

// // Initialize Firebase
// const app = initializeApp(firebaseConfig);
