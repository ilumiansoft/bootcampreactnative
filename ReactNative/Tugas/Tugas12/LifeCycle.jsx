import React, { useEffect, useState } from 'react';
import { View, Text, StyleSheet } from 'react-native';

export default function LifeCycle() {
    const [incCount, setIncCount] = useState(0);
    const [decCount, setDecCount] = useState(3000);
    useEffect(() => {
        const timerRef = setInterval(() => {
            setIncCount(prevIncCount => prevIncCount + 1);
            setDecCount(prevDecCount => prevDecCount - 1);
        }, 1000);

        return () => {
            clearInterval(timerRef)
        }
    }, []);

    return (
        <View style={styles.container}
        >
            <View style={styles.container}>
                <View style={styles.boxContainer}>
                    <Text>Counter 1 :</Text>
                    <Text>{incCount}</Text>
                </View>
                <View style={styles.boxContainer}>
                    <Text>Counter 2 :</Text>
                    <Text>{decCount}</Text>
                </View>
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 20,
        justifyContent: "center",
    },
    boxContainer: {
        flexDirection: "row",
        justifyContent: "space-between",
        borderWidth: 1,
        borderRadius: 8,
        padding: 10,
        borderColor: "#eaeaea",
        marginVertical: 5,
    },
});
