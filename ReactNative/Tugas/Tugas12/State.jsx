import {
    Button,
    FlatList,
    StyleSheet,
    Text,
    TextInput,
    TouchableOpacity,
    View,
} from "react-native";
import React, { useState } from "react";

export default function ListMovie() {
    const [movie, setMovie] = useState([
        { id: "0", name: "Harry Potter", lengthOfTime: "120" },
        { id: "1", name: "Sherlock Holmes", lengthOfTime: "125" },
    ]);

    const [name, setName] = useState("");
    const [lengthOfTime, setLengthOfTime] = useState(0);
    const [selectedMovieId, setSelectedMovieId] = useState("");

    const clearForm = () => {
        setName("");
        setLengthOfTime("");
        setSelectedMovieId("");
    };

    const onAdd = () => {
        setMovie((prevMovie) => [
            // Salin semua data movie menggunakan operator spread
            ...prevMovie,
            // Tambahakan data object baru pada bagian akhir array
            {
                id: Math.random().toString(),
                name,
                lengthOfTime,
            },
        ]);
        clearForm();
    };

    const onUpdate = () => {
        setMovie((prevMovie) =>
            // Method map akan melakukan looping pada arary
            // kemudian melakukan modifikasi pada tiap element
            // dan mengembalikan array hasil modifikasinya
            prevMovie.map((item) => {
                // Jika item merupakan data yang kita pilih
                // update item dengan data dari form
                if (item.id === selectedMovieId) {
                    return {
                        id: item.id,
                        name,
                        lengthOfTime,
                    };
                }

                // Jika bukan terpilih kembalikan item tanpa perubahan
                return item;
            }),
        );
        clearForm();
    };

    const onDelete = (id) => {
        const deletedArr = movie.filter((delItem) => delItem.id != id);
        setMovie((prevMovie) => deletedArr
        );
        clearForm();
    };

    return (
        <View style={styles.container}>
            <FlatList
                data={movie}
                renderItem={({ item }) => (
                    <View style={styles.listContainer}>
                        <TouchableOpacity
                            onPress={() => {
                                setSelectedMovieId(item.id);
                                setName(item.name);
                                setLengthOfTime(item.lengthOfTime);
                            }}
                            style={styles.listText}>
                            <Text>Name : {item.name}</Text>
                            <Text>Length of Time : {item.lengthOfTime}</Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                            style={styles.buttonBox}
                            onPress={() => {
                                onDelete(item.id)
                            }}>
                            <Text style={styles.buttonText}>DELETE</Text>
                        </TouchableOpacity>
                    </View>

                )}
            />
            <View>
                <TextInput
                    style={styles.formInputText}
                    value={name}
                    onChangeText={setName}
                    placeholder="Input Movie Title"
                />
                <TextInput
                    style={styles.formInputText}
                    keyboardType="numeric"
                    value={lengthOfTime}
                    onChangeText={setLengthOfTime}
                    placeholder="Input Movie Length"
                />
                <Button
                    title={!selectedMovieId ? "Add Movie" : "Update Movie"}
                    onPress={!selectedMovieId ? onAdd : onUpdate}
                />
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 20,
    },
    listContainer: {
        flexDirection: "row",
        justifyContent: "space-between",
        borderWidth: 1,
        borderRadius: 8,
        borderColor: "#eaeaea",
        marginVertical: 5,
    },
    formInputText: {
        alignItems: "center",
        borderColor: "#eaeaea",
        borderRadius: 4,
        borderWidth: 1,
        color: "#7d7d7d",
        flexDirection: "row",
        fontSize: 18,
        height: 54,
        justifyContent: "center",
        marginVertical: 8,
        paddingHorizontal: 10,
    },
    buttonBox: {
        elevation: 10,
        justifyContent: "center",
        height: 50,
        width: 80,
        margin: 12,
        padding: 10,
        backgroundColor: "#2196F3",
        borderRadius: 3,
    },
    buttonText: {
        alignSelf: "center",
        fontSize: 16,
        color: "white",
    },
    listText: {
        padding: 10,
        marginVertical: 5,
    },
});