import React, { useEffect } from "react";
import { useState } from "react";
import { StyleSheet, Text, View, Image, Button } from "react-native";
import { FlatList, TouchableOpacity } from "react-native-gesture-handler";
import { Data } from "./data";
export default function Home({ route, navigation }) {
  const { username } = route.params;
  const [totalPrice, setTotalPrice] = useState(0);
  const [currentPrice, setCurrentPrice] = useState(0);

  const currencyFormat = (num) => {
    return "Rp " + num.toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.");
  };

  const updateHarga = (price) => {
    console.log("UpdatPrice : " + price);
    const temp = Number(price);
    setTotalPrice((prevTotalPrice) => prevTotalPrice + temp);
    //? #Bonus (10 poin) -- HomeScreen.js --
    //? agar harga dapat update misal di tambah lebih dari 1 item atau lebih -->
  };
  return (
    <View style={styles.container}>
      <View
        style={{
          flexDirection: "row",
          justifyContent: "space-between",
          padding: 16,
        }}
      >
        <View>
          <Text>Selamat Datang,</Text>
          <Text style={{ fontSize: 18, fontWeight: "bold" }}>{username}</Text>
        </View>
        <View>
          <Text>Total Harga:</Text>
          <Text style={{ fontSize: 18, fontWeight: "bold" }}>
            {" "}
            {currencyFormat(totalPrice)}
          </Text>
        </View>
      </View>
      <View
        style={{ alignItems: "center", marginBottom: 20, paddingBottom: 60 }}
      >
        {/* //? #Soal No 2 (15 poin) -- Home.js -- Function Home
            //? Buatlah 1 komponen FlatList dengan input berasal dari data.js   
            //? dan memiliki 2 kolom, sehingga menampilkan 2 item per baris (horizontal) -->

            //? #Soal No 3 (15 poin) -- HomeScreen.js --
             //? Buatlah styling komponen Flatlist, agar dapat tampil dengan baik di device untuk layouting bebas  --> */}
        <FlatList
          columnWrapperStyle={{
            flex: 1,
            justifyContent: "space-evenly",
          }}
          data={Data}
          horizontal={false}
          numColumns={2}
          renderItem={({ item }) => (
            <TouchableOpacity
              style={styles.content}
              onPress={() => {
                setCurrentPrice(item.harga);
                updateHarga(currentPrice);
                console.log(currentPrice);
              }}
            >
              <Text style={styles.itemType}>{item.type}</Text>
              <Image style={styles.itemImage} source={item.image} />
              <Text style={styles.itemTitle}>{item.title}</Text>
              <Text style={styles.itemDesc}>{item.desc}</Text>
              <Text style={styles.itemPrice}>Rp. {item.harga}</Text>
            </TouchableOpacity>
          )}
        />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white",
  },
  content: {
    flex: 0.5,
    width: 150,
    height: 220,
    margin: 5,
    borderWidth: 1,
    alignItems: "center",
    borderRadius: 5,
    borderColor: "grey",
  },
  itemImage: {
    flex: 3,
    margin: 5,
    height: 130,
    width: 130,
    borderRadius: 5,
  },
  itemType: {
    alignSelf: "flex-end",
    marginHorizontal: 10,
    flex: 0.4,
    fontSize: 10,
    fontWeight: "bold",
  },
  itemTitle: {
    flex: 0.6,
    fontSize: 16,
    fontWeight: "bold",
  },
  itemDesc: {
    flex: 2,
    flexWrap: "wrap",
    fontSize: 10,
    marginRight: 5,
  },
  itemPrice: {
    flex: 1,
    alignSelf: "flex-end",
    marginHorizontal: 10,
    fontSize: 16,
    fontWeight: "bold",
  },
});
