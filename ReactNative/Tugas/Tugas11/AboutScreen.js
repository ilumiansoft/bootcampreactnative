import React, { useContext, useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  ToastAndroid,
} from "react-native";
import { CreContext } from "../Tugas14/CredentialContext";
import * as firebase from "firebase";

const colors = {
  biru: "#15869E",
  putih: "#E5E5E5",
  birutua: "#165E6D",
  pureputih: "#FFFFFF",
};

export default function AboutScreen({ navigation }) {
  const { userState, setUserState } = useContext(CreContext);

  const onLogout = () => {
    firebase
      .auth()
      .signOut()
      .then(() => {
        console.log("logout berhasil");
        setUserState({ isLogin: false, name: "", password: "" });
        ToastAndroid.show("Berhasil Logout", ToastAndroid.SHORT);
      })
      .catch((e) => {
        console.log("logout gagal " + e);
        ToastAndroid.show("Logout Gagal", ToastAndroid.SHORT);
      });
  };
  return (
    <View
      style={[
        styles.container,
        {
          flexDirection: "column",
          justifyContent: "flex-start",
        },
      ]}
    >
      {/* header atas */}
      <View
        style={styles.container}
        flexDirection="row"
        justifyContent="space-between"
      >
        <View style={{ flex: 3 }}>
          <Text style={styles.headerText}>DEVELOPER PROFILE</Text>
          <View style={styles.lineBetween} />
        </View>

        <TouchableOpacity style={styles.buttonBox} onPress={onLogout}>
          <Image source={require("./Image/back.png")} alignSelf="center" />
        </TouchableOpacity>
      </View>
      {/* foto profil */}
      <View style={styles.container}>
        <Image style={styles.fotoProfil} source={require("./Image/user.png")} />
        <Text style={styles.nameText}>{userState["name"]}</Text>
        <Text style={styles.profText}>GAME DEVELOPER</Text>
      </View>
      {/* skill set */}
      <View style={styles.container2}>
        <View>
          <Text style={styles.headerText}>SKILL LIST</Text>
          <View style={styles.lineBetween} />
          <Image
            style={{ marginTop: 10, alignSelf: "center" }}
            source={require("./Image/listSkill.png")}
          />
        </View>
      </View>
      {/* programming skill */}
      <View style={styles.container}>
        <View>
          <Text style={styles.headerText}>PROGRAMMING SKILL</Text>
          <View style={styles.lineBetween} />
          <Image
            style={{ marginTop: 10, alignSelf: "center" }}
            source={require("./Image/progskill.png")}
          />
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.pureputih,
  },
  container2: {
    flex: 2,
    backgroundColor: colors.pureputih,
  },
  fotoProfil: {
    alignSelf: "center",
  },
  headerText: {
    fontSize: 18,
    fontWeight: "bold",
    color: colors.biru,
    marginHorizontal: 20,
    marginTop: 30,
  },
  nameText: {
    alignSelf: "center",
    fontSize: 14,
    fontWeight: "bold",
    color: colors.biru,
    marginHorizontal: 20,
    marginTop: 10,
  },
  profText: {
    alignSelf: "flex-end",
    fontSize: 12,
    fontWeight: "100",
    color: colors.biru,
    marginHorizontal: 20,
    marginRight: 100,
  },
  lineBetween: {
    borderBottomColor: colors.biru,
    borderBottomWidth: 3,
    marginLeft: 20,
    width: "70%",
  },
  buttonBox: {
    height: 50,
    width: 50,
    margin: 12,
    padding: 10,
  },
});
