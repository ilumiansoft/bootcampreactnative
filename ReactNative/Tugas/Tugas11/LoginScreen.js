import React, { useContext, useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  TouchableOpacity,
  ToastAndroid,
} from "react-native";

import { CreContext } from "../Tugas14/CredentialContext";
import * as firebase from "firebase";

const colors = {
  biru: "#15869E",
  putih: "#E5E5E5",
  birutua: "#165E6D",
  pureputih: "#FFFFFF",
};

const handleinput = (text) => {
  setName(text);
};

export default function LoginScreen({ navigation }) {
  const { userState, setUserState } = useContext(CreContext);
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const firebaseConfig = {
    apiKey: "AIzaSyBZ9OFzTBsJZBhpD7t44ZKXv-UMEZyM5tE",
    authDomain: "jcc-batch2-latihanauth.firebaseapp.com",
    projectId: "jcc-batch2-latihanauth",
    storageBucket: "jcc-batch2-latihanauth.appspot.com",
    messagingSenderId: "567229358169",
    appId: "1:567229358169:web:8639abb106fc4776d7d148",
  };

  const onLogin = () => {
    if (email != "" && password != "") {
      firebase
        .auth()
        .signInWithEmailAndPassword(email, password)
        .then(() => {
          setUserState({
            isLogin: true,
            name: userState["name"],
            password: userState["password"],
          });
          console.log("login berhasil");
        })
        .catch((e) => {
          console.log("login gagal " + e);
          ToastAndroid.show("Email atau password salah", ToastAndroid.SHORT);
        });
    } else {
      ToastAndroid.show(
        "Pastikan kolom login dan password telah terisi",
        ToastAndroid.SHORT
      );
    }
  };

  const onRegister = () => {
    if (email != "" && password != "") {
      firebase
        .auth()
        .createUserWithEmailAndPassword(email, password)
        .then(() => {
          ToastAndroid.show(
            "Register berhasil, silahkan login untuk melanjutkan",
            ToastAndroid.SHORT
          );
          setEmail("");
          setPassword("");
          console.log("register berhasil");
        })
        .catch((e) => {
          console.log("register gagal " + e);
          ToastAndroid.show(
            "Akun sudah terdaftar / format email salah/ password minimal 6 karakter",
            ToastAndroid.SHORT
          );
        });
    } else {
      ToastAndroid.show(
        "Pastikan kolom login dan password telah terisi",
        ToastAndroid.SHORT
      );
    }
  };

  if (!firebase.apps.length) {
    firebase.initializeApp(firebaseConfig);
  }

  return (
    <View
      style={[
        styles.container,
        {
          flexDirection: "column",
        },
      ]}
    >
      <Image style={styles.loginLogo} source={require("./Image/logo.png")} />
      <View style={styles.container}>
        <Text style={styles.headerText}>PLEASE LOGIN TO CONTINUE</Text>
        <TextInput
          style={styles.input}
          placeholder="Email"
          value={email}
          onChangeText={(email) => setEmail(email)}
        />
        <TextInput
          style={styles.input}
          placeholder="Password"
          value={password}
          onChangeText={(password) => setPassword(password)}
        />
        <TouchableOpacity style={styles.buttonBox} onPress={onLogin}>
          <Text style={styles.buttonText}>LOGIN</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.buttonBox2} onPress={onRegister}>
          <Text style={styles.buttonText}>REGISTER</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.putih,
  },
  loginLogo: {
    marginVertical: 100,
    resizeMode: "contain",
    alignSelf: "center",
  },
  headerText: {
    fontSize: 14,
    fontWeight: "bold",
    color: colors.biru,
    marginHorizontal: 10,
  },
  input: {
    height: 40,
    margin: 12,
    borderWidth: 1,
    borderColor: colors.birutua,
    padding: 10,
    marginTop: 20,
  },
  buttonBox: {
    height: 40,
    margin: 12,
    backgroundColor: colors.biru,
    padding: 10,
    marginTop: 50,
  },
  buttonBox2: {
    height: 40,
    margin: 12,
    backgroundColor: colors.biru,
    padding: 10,
    marginTop: 10,
  },
  buttonText: {
    fontSize: 14,
    fontWeight: "bold",
    alignSelf: "center",
    color: colors.putih,
    marginHorizontal: 10,
  },
});
