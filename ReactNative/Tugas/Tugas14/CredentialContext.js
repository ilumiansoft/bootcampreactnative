import React, { useState, createContext } from "react";

export const CreContext = createContext();

export const CreProvider = ({ children }) => {
  const [userState, setUserState] = useState({
    name: "",
    password: "",
    isLogin: false,
  });

  return (
    <CreContext.Provider value={{ userState, setUserState }}>
      {children}
    </CreContext.Provider>
  );
};
