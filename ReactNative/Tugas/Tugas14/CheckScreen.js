import React, { useContext, useState } from "react";
import { Text, View } from "react-native";
import { createStackNavigator } from "@react-navigation/stack";
import { NavigationContainer } from "@react-navigation/native";
import { CreContext } from "../Tugas14/CredentialContext";
import LoginScreen from "../Tugas11/LoginScreen";
import AboutScreen from "../Tugas11/AboutScreen";
import { NewsListScreen } from "../Tugas15/Features/News";

const Stack = createStackNavigator();

export default function CheckScreen({ navigation }) {
  const { userState, setUserState } = useContext(CreContext);
  const isLogin = userState["isLogin"];

  return (
    <NavigationContainer>
      <Stack.Navigator>
        {isLogin ? (
          <Stack.Screen name="About" component={AboutScreen} />
        ) : (
          <Stack.Screen name="Login" component={LoginScreen} />
        )}
      </Stack.Navigator>
    </NavigationContainer>
  );
}
