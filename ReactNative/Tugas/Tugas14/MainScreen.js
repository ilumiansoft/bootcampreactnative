import React, { useContext, useState } from "react";
import { CreProvider } from "./CredentialContext";
import LoginScreen from "../Tugas11/LoginScreen";
import AboutScreen from "../Tugas11/AboutScreen";
import CheckScreen from "./CheckScreen";

export default function MainScreen() {
  return (
    <CreProvider>
      <CheckScreen />
    </CreProvider>
  );
}
