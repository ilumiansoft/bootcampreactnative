import React from "react";
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
} from "react-native";

const colors = {
  biru: "#15869E",
  putih: "#E5E5E5",
  birutua: "#165E6D",
  pureputih: "#FFFFFF",
};

export default function LoginScreen({ navigation }) {
  return (
    <View
      style={[
        styles.container,
        {
          flexDirection: "column",
        },
      ]}
    >
      <View style={styles.container}>
        <Text style={styles.headerText}>PLEASE LOGIN TO CONTINUE</Text>
        <TextInput style={styles.input} placeholder="Username" />
        <TextInput style={styles.input} placeholder="Password" />
        <TouchableOpacity
          style={styles.buttonBox}
          onPress={() => navigation.navigate("MyDrawwer")}
        >
          <Text style={styles.buttonText}>LOGIN</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.putih,
    justifyContent: "center",
  },
  headerText: {
    fontSize: 14,
    fontWeight: "bold",
    color: colors.biru,
    marginHorizontal: 10,
  },
  input: {
    height: 40,
    margin: 12,
    borderWidth: 1,
    borderColor: colors.birutua,
    padding: 10,
    marginTop: 20,
  },
  buttonBox: {
    height: 40,
    margin: 12,
    backgroundColor: colors.biru,
    padding: 10,
    marginTop: 50,
  },
  buttonText: {
    fontSize: 14,
    fontWeight: "bold",
    alignSelf: "center",
    color: colors.putih,
    marginHorizontal: 10,
  },
});
