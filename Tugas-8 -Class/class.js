///////////////////////////////////////////////////////////////////////
////                            soal 1                             ////
///////////////////////////////////////////////////////////////////////

class Animal {
  constructor(name) {
    this.name = name;
    this.legs = 4;
    this.cold_blooded = false;
  }
  get name() {
    return this._name;
  }
  set name(input) {
    this._name = input;
  }
  get legs() {
    return this._legs;
  }
  set legs(input) {
    this._legs = input;
  }
  get cold_blooded() {
    return this._cold_blooded;
  }
  set cold_blooded(input) {
    this._cold_blooded = input;
  }
}

const sheep = new Animal("shaun");

console.log(sheep.name); // "shaun"
console.log(sheep.legs); // 4
console.log(sheep.cold_blooded); // false
console.log("=====================================================");

class Ape extends Animal {
  constructor(name) {
    super(name);
    this.legs = 2;
  }

  get name() {
    return this._name;
  }
  set name(input) {
    this._name = input;
  }
  get legs() {
    return this._legs;
  }
  set legs(input) {
    this._legs = input;
  }
  get cold_blooded() {
    return this._cold_blooded;
  }
  set cold_blooded(input) {
    this._cold_blooded = input;
  }

  yell() {
    console.log("Auoo");
  }
}

class Frog extends Animal {
  constructor(name) {
    super(name);
    this.cold_blooded = true;
  }

  get name() {
    return this._name;
  }
  set name(input) {
    this._name = input;
  }
  get legs() {
    return this._legs;
  }
  set legs(input) {
    this._legs = input;
  }
  get cold_blooded() {
    return this._cold_blooded;
  }
  set cold_blooded(input) {
    this._cold_blooded = input;
  }

  jump() {
    console.log("Hop hop");
  }
}

const sungokong = new Ape("kera sakti");
console.log(sungokong.name); // "kera sakti"
console.log(sungokong.legs); // 2
console.log(sungokong.cold_blooded); // false
sungokong.yell(); // "Auooo"
console.log("=====================================================");

const kodok = new Frog("buduk");
console.log(kodok.name); // "buduk"
console.log(kodok.legs); // 4
console.log(kodok.cold_blooded); // true
kodok.jump(); // "hop hop"
console.log("======================end soal 1======================");

///////////////////////////////////////////////////////////////////////
////                            soal 2                             ////
///////////////////////////////////////////////////////////////////////

class Clock {
  constructor({ template }) {
    this._template = template;
    this.timer;
  }

  get template() {
    return this._template;
  }
  set template(input) {
    this._template = input;
  }

  render() {
    var date = new Date();

    var hours = date.getHours();
    if (hours < 10) hours = "0" + hours;

    var mins = date.getMinutes();
    if (mins < 10) mins = "0" + mins;

    var secs = date.getSeconds();
    if (secs < 10) secs = "0" + secs;

    var output = clock.template
      .replace("h", hours)
      .replace("m", mins)
      .replace("s", secs);

    console.log(output);
  }

  stop() {
    clearInterval(timer);
  }

  start() {
    clock.render();
    this.timer = setInterval(clock.render, 1000);
  }
}

var clock = new Clock({ template: "h:m:s" });
clock.start();
