////////////////////////////////////////////////////////////////////////////////////
////                                  soal1                                     ////
////////////////////////////////////////////////////////////////////////////////////

function replaceCharacter(kalimat, target, replace) {
  var array = kalimat.split("");
  for (count = 0; count < array.length; count++) {
    if (array[count] == target) {
      if (replace != null) {
        array.splice(count, 1, replace);
      } else {
        array.splice(count, 1);
      }
    }
  }
  var string = array.join("");
  return string;
}

var sentence = "Pada Hari Minggu ku turut ayah ke kota";

var result = replaceCharacter(sentence, "a", "o");
console.log(result); //Podo Hori Minggu ku turut oyoh ke koto
console.log("====================================================");

////////////////////////////////////////////////////////////////////////////////////
////                                  soal2                                     ////
////////////////////////////////////////////////////////////////////////////////////

function sortGrade(data) {
  var sortNum = data.sort(function (a, b) {
    return a[0] < b[0] ? 1 : -1;
  });
  var hasil = sortNum.sort(function (c, d) {
    return d[2] > c[2] ? 1 : -1;
  });
  return hasil;
}

var studentData = [
  [2, "John Duro", 60],
  [4, "Robin Ackerman", 100],
  [1, "Jaeger Marimo", 60],
  [6, "Zoro", 80],
  [5, "Zenitsu", 80],
  [3, "Patrick Zala", 90],
];

var sortedData = sortGrade(studentData);
console.log(sortedData);
