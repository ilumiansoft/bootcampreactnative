////////////////////////////////////////////////////////////////////////
////                           soal 1                               ////
////////////////////////////////////////////////////////////////////////

const golden = () => {
  console.log("this is golden!!");
};

golden();
console.log("=======================end soal 1=========================");

////////////////////////////////////////////////////////////////////////
////                           soal 2                               ////
////////////////////////////////////////////////////////////////////////

const newFunction = (firstName, lastName) => {
  return {
    firstName,
    lastName,
    fullName: () => {
      console.log(firstName + " " + lastName);
      return;
    },
  };
};

const person = newFunction("William", "Imoh");
console.log(person);
person.fullName();
console.log("=======================end soal 2=========================");

////////////////////////////////////////////////////////////////////////
////                           soal 3                               ////
////////////////////////////////////////////////////////////////////////

const newObject = {
  firstName: "Harry",
  lastName: "Potter Holt",
  destination: "Hogwarts React Conf",
  occupation: "Deve-wizard Avocado",
  spell: "Vimulus Renderus!!!",
};

const { firstName, lastName, destination, occupation } = newObject;

console.log(firstName, lastName, destination, occupation);
console.log("=======================end soal 3=========================");

////////////////////////////////////////////////////////////////////////
////                           soal 4                               ////
////////////////////////////////////////////////////////////////////////

const west = ["Will", "Chris", "Sam", "Holly"];
const east = ["Gill", "Brian", "Noel", "Maggie"];
const combined = [...west, ...east];

console.log(combined);
console.log("=======================end soal 4=========================");

////////////////////////////////////////////////////////////////////////
////                           soal 5                               ////
////////////////////////////////////////////////////////////////////////

const planet = "earth";
const view = "glass";
var before = `Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet} do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam`;

console.log(before);
console.log("=======================end soal 5=========================");
