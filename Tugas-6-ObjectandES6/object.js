////////////////////////////////////////////////////////////////////////
////                           soal 1                               ////
////////////////////////////////////////////////////////////////////////

function arrayToObject(params) {
  var container = {};
  var now = new Date();
  var year = now.getFullYear();

  for (var count = 0; count <= params.length - 1; count++) {
    var localCont = {};
    localCont.firstName = params[count][0];
    localCont.lastName = params[count][1];
    localCont.gender = params[count][2];
    if (params[count][3] != null) {
      if (params[count][3] > year) {
        localCont.age = "invalid birth year";
      } else {
        localCont.age = year - params[count][3];
      }
    } else {
      localCont.age = "invalid birth year";
    }
    var name =
      count + 1 + "." + " " + params[count][0] + " " + params[count][1];
    container[name] = localCont;
  }

  return console.log(container);
}

var people = [
  ["Bruce", "Banner", "male", 1975],
  ["Natasha", "Romanoff", "female"],
];
arrayToObject(people);

var people2 = [
  ["Tony", "Stark", "male", 1980],
  ["Pepper", "Pots", "female", 2023],
];
arrayToObject(people2);
arrayToObject([]);
console.log("=======================end soal 1=========================");

////////////////////////////////////////////////////////////////////////
////                           soal 2                               ////
////////////////////////////////////////////////////////////////////////

function shoppingTime(memberId, money) {
  var container = {};

  if (memberId == "" || memberId == null) {
    container = "Mohon maaf, toko X hanya berlaku untuk member saja";
  } else if (money < 50000) {
    container = "Mohon maaf, uang tidak cukup";
  } else {
    var localCont = [];
    container.memberId = memberId;
    container.money = money;
    if (money >= 1500000) {
      localCont.push("Sepatu brand Stacattu");
      money -= 1500000;
    }
    if (money >= 500000) {
      localCont.push("Baju brand Zoro");
      money -= 500000;
    }
    if (money >= 250000) {
      localCont.push("Baju brand H&N");
      money -= 250000;
    }
    if (money >= 175000) {
      localCont.push("Sweater brand Uniklooh");
      money -= 175000;
    }
    if (money >= 50000) {
      localCont.push("Casing Handphone");
      money -= 50000;
    }

    container.listPurchased = localCont;
    container.changeMoney = money;
  }

  return container;
}

console.log(shoppingTime("1820RzKrnWn08", 2475000));
console.log(shoppingTime("82Ku8Ma742", 170000));
console.log(shoppingTime("", 2475000));
console.log(shoppingTime("234JdhweRxa53", 15000));
console.log(shoppingTime());
console.log("=======================end soal 2=========================");

////////////////////////////////////////////////////////////////////////
////                           soal 3                               ////
////////////////////////////////////////////////////////////////////////

function naikAngkot(listPenumpang) {
  var rute = ["A", "B", "C", "D", "E", "F"];
  var container = [];
  for (var count = 0; count <= listPenumpang.length - 1; count++) {
    var localCont = {};
    if (
      rute.includes(listPenumpang[count][1]) == true &&
      rute.includes(listPenumpang[count][2]) == true
    ) {
      var lokAwal = rute.indexOf(listPenumpang[count][1]) + 1;
      var lokAkhir = rute.indexOf(listPenumpang[count][2]) + 1;
      var harga = (lokAkhir - lokAwal) * 2000;
    }
    localCont.penumpang = listPenumpang[count][0];
    localCont.naikDari = listPenumpang[count][1];
    localCont.tujuan = listPenumpang[count][2];
    localCont.bayar = harga;
    container.push(localCont);
  }
  return container;
}

console.log(
  naikAngkot([
    ["Dimitri", "B", "F"],
    ["Icha", "A", "B"],
  ])
);
console.log("=======================end soal 3=========================");
